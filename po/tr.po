# Turkish translation of gspell.
# Copyright (C) 2002-2003, 2004, 2005 Free Software Foundation, Inc.
# Copyright (C) 2006-2022 gspell's COPYRIGHT HOLDER
# This file is distributed under the same license as the gspell package.
#
# Nilgün Belma Bugüner <nilgun@fide.org>, 2001, 2002.
# Fatih Demir <kabalak@gtranslator.org>, 2000.
# Baris Cicek <baris@teamforce.name.tr>, 2004, 2005, 2008, 2009.
# Rıdvan CAN <ridvan@gamebox.net>, 2004.
# Gökhan Gurbetoğlu <ggurbet@gmail.com>, 2014, 2015.
# sabri ünal <yakushabb@gmail.com>, 2014.
# Necdet Yücel <necdetyucel@gmail.com>, 2015.
# Muhammet Kara <muhammetk@gmail.com>, 2011, 2012, 2013, 2014, 2015.
# Emin Tufan Çetin <etcetin@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gspell\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gspell/issues\n"
"POT-Creation-Date: 2023-04-30 20:23+0200\n"
"PO-Revision-Date: 2017-09-06 09:51+0300\n"
"Last-Translator: Emin Tufan Çetin <etcetin@gmail.com>\n"
"Language-Team: Türkçe <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"
"X-POOTLE-MTIME: 1433962102.000000\n"

#: gspell/gspell-checker.c:415
#, c-format
msgid "Error when checking the spelling of word “%s”: %s"
msgstr "“%s” sözcüğünün yazım denetimi yapılırken hata: %s"

#. Translators: Displayed in the "Check Spelling"
#. * dialog if there are no suggestions for the current
#. * misspelled word.
#.
#. No suggestions. Put something in the menu anyway...
#: gspell/gspell-checker-dialog.c:150 gspell/gspell-context-menu.c:217
msgid "(no suggested words)"
msgstr "(sözcük önerisi yok)"

#: gspell/gspell-checker-dialog.c:235
msgid "Error:"
msgstr "Hata:"

#: gspell/gspell-checker-dialog.c:271
msgid "Completed spell checking"
msgstr "Yazım denetimi tamamlandı"

#: gspell/gspell-checker-dialog.c:275
msgid "No misspelled words"
msgstr "Hatalı yazılmış sözcük yok"

#. Translators: Displayed in the "Check
#. * Spelling" dialog if the current word
#. * isn't misspelled.
#.
#: gspell/gspell-checker-dialog.c:502
msgid "(correct spelling)"
msgstr "(imla hatalarını düzelt)"

#: gspell/gspell-checker-dialog.c:644
msgid "Suggestions"
msgstr "Öneriler"

#: gspell/gspell-context-menu.c:152
msgid "_Language"
msgstr "_Dil"

#: gspell/gspell-context-menu.c:240
msgid "_More…"
msgstr "_Daha çok…"

#. Ignore all
#: gspell/gspell-context-menu.c:285
msgid "_Ignore All"
msgstr "Hepsini _Yoksay"

#. Add to Dictionary
#: gspell/gspell-context-menu.c:303
msgid "_Add"
msgstr "_Ekle"

#: gspell/gspell-context-menu.c:340
msgid "_Spelling Suggestions…"
msgstr "_Yazım Önerileri…"

#. Translators: %s is the language ISO code.
#: gspell/gspell-language.c:89
#, c-format
msgctxt "language"
msgid "Unknown (%s)"
msgstr "Bilinmeyen (%s)"

#: gspell/gspell-language-chooser-button.c:84
msgid "No language selected"
msgstr "Dil seçilmedi"

#: gspell/gspell-navigator-text-view.c:310
msgid ""
"Spell checker error: no language set. It’s maybe because no dictionaries are "
"installed."
msgstr ""
"Yazım denetçisi hatası: ayarlanan dil yok. Bunun nedeni belki de hiçbir "
"sözlüğün kurulu olmamasıdır."

#: gspell/resources/checker-dialog.ui:7
msgid "Check Spelling"
msgstr "Yazım Denetimi Yap"

#: gspell/resources/checker-dialog.ui:36
msgid "Misspelled word:"
msgstr "Hatalı yazılmış sözcük:"

#: gspell/resources/checker-dialog.ui:49
msgid "word"
msgstr "sözcük"

#: gspell/resources/checker-dialog.ui:66
msgid "Change _to:"
msgstr "Değiş_tir:"

#: gspell/resources/checker-dialog.ui:91
msgid "Check _Word"
msgstr "Sözcük De_netle"

#: gspell/resources/checker-dialog.ui:120
msgid "_Suggestions:"
msgstr "_Öneriler:"

#: gspell/resources/checker-dialog.ui:133
msgid "_Ignore"
msgstr "_Yoksay"

#: gspell/resources/checker-dialog.ui:146
msgid "Ignore _All"
msgstr "Tü_münü Yoksay"

#: gspell/resources/checker-dialog.ui:159
msgid "Cha_nge"
msgstr "_Değiştir"

#: gspell/resources/checker-dialog.ui:174
msgid "Change A_ll"
msgstr "_Tümünü Değiştir"

#: gspell/resources/checker-dialog.ui:191
msgid "User dictionary:"
msgstr "Kullanıcı sözlüğü:"

#: gspell/resources/checker-dialog.ui:203
msgid "Add w_ord"
msgstr "S_özcük ekle"

#: gspell/resources/language-dialog.ui:7
msgid "Set Language"
msgstr "Dili Ayarla"

#: gspell/resources/language-dialog.ui:20
msgid "Select the spell checking _language."
msgstr "Yazım denetimi _dilini seç."

#: gspell/resources/language-dialog.ui:61
msgid "_Cancel"
msgstr "_İptal"

#: gspell/resources/language-dialog.ui:68
msgid "_Select"
msgstr "_Seç"
